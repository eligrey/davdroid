/*
 * Copyright (c) Ricki Hirner (bitfire web engineering).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

apply plugin: 'com.android.application'
apply plugin: 'kotlin-android'
apply plugin: 'kotlin-android-extensions'
apply plugin: 'org.jetbrains.dokka-android'

android {
    compileSdkVersion 27
    buildToolsVersion '28.0.2'

    defaultConfig {
        applicationId "org.oftn.davdroid"

        versionCode 245
        buildConfigField "long", "buildTime", System.currentTimeMillis() + "L"
        buildConfigField "boolean", "customCerts", "true"

        minSdkVersion 19        // Android 4.4
        targetSdkVersion 27     // Android 8.1

        // when using this, make sure that notification icons are real bitmaps
        vectorDrawables.useSupportLibrary = true
    }

    flavorDimensions "type"
    productFlavors {
        standard {
            versionName "2.0.4-ose"

            buildConfigField "boolean", "customCerts", "true"
        }
    }

    buildTypes {
        debug {
            minifyEnabled false
        }
        release {
            minifyEnabled true
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.txt'
        }
    }

    lintOptions {
        disable 'GoogleAppIndexingWarning'      // we don't need Google indexing, thanks
        disable 'ImpliedQuantity', 'MissingQuantity'		// quantities from Transifex may vary
        disable 'MissingTranslation', 'ExtraTranslation'	// translations from Transifex are not always up to date
        disable "OnClick"     // doesn't recognize Kotlin onClick methods
        disable 'RtlEnabled'
        disable 'RtlHardcoded'
        disable 'Typos'
    }
    packagingOptions {
        exclude 'META-INF/DEPENDENCIES'
        exclude 'META-INF/LICENSE'
    }

    defaultConfig {
        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"
    }
}

dependencies {
    implementation project(':cert4android')
    implementation project(':dav4android')
    implementation project(':ical4android')
    implementation project(':vcard4android')

    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"

    implementation 'com.android.support:appcompat-v7:27.1.1'
    implementation 'com.android.support:cardview-v7:27.1.1'
    implementation 'com.android.support:design:27.1.1'
    implementation 'com.android.support:preference-v14:27.1.1'

    implementation 'com.github.yukuku:ambilwarna:2.0.1'
    implementation 'com.mikepenz:aboutlibraries:6.0.9'

    implementation 'com.squareup.okhttp3:logging-interceptor:3.11.0'
    implementation 'commons-io:commons-io:2.6'
    implementation 'dnsjava:dnsjava:2.1.8'
    implementation 'org.apache.commons:commons-lang3:3.7'
    implementation 'org.apache.commons:commons-collections4:4.1'

    // for tests
    androidTestImplementation 'com.android.support.test:runner:1.0.2'
    androidTestImplementation 'com.android.support.test:rules:1.0.2'
    androidTestImplementation 'junit:junit:4.12'
    androidTestImplementation 'com.squareup.okhttp3:mockwebserver:3.11.0'

    testImplementation 'junit:junit:4.12'
    testImplementation 'com.squareup.okhttp3:mockwebserver:3.11.0'
}
